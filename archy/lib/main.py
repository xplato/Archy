import sys
import time

from .config import Config
from .parser import Parser
from .archiver import Archiver

class Archy:
	def __init__(self, config, settings):
		self.config = config
		self.settings = settings

		self.parser = Parser()
		self.archiver = Archiver(config, settings)
	
	def __call__(self):
		self.parser.parse(self.config.get_config())
		self.archiver()