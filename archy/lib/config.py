import os
import json
from subprocess import call
from platform import system
from pathlib import Path


default_conf = {
	"files": [],
	"folders": [],
}

class Config:
	def __init__(self, DEV=False):
		self.filename = 'archy.json'
		self.config_location = os.path.join(Path.home(), '.config/archy')
		self.config_file = os.path.join(self.config_location, self.filename)

		if DEV:
			self.filename = 'archy-dev.json'
			self.config_file = os.path.join(self.config_location, self.filename)

	## Creation and Deletion
	def _create_config_file(self):
		if not os.path.exists(self.config_location):
			os.makedirs(self.config_location)

		try:
			with open(self.config_file, 'x') as archy_conf:
				archy_conf.write(json.dumps(default_conf))
				
			print(f'Created {self.config_file}')
			print('Use "archy -c edit" to modify it.')
		except FileExistsError: # you tell me
			pass

	def _check_config(self):
		if os.path.exists(self.config_file):
			print('It appears that you already have a configuration file.')
			print('You can use "archy -c delete" to remove it or "archy -c echo" to see it.')
			quit()
		
		self._create_config_file()

	def _reset_configuration_file(self):
		os.remove(self.config_file)
		
		if input("Create a new configuration file? (y/n) ") == "y":
			self.create()
		else:
			quit()

	def _confirm_reset(self):
		if input(f'Are you sure? This will delete {self.filename} (y/n) ') == 'y':
			self._reset_configuration_file()
		else:
			quit()


	## Handlers
	def get_config(self):
		try:
			with open(self.config_file, "r") as conf_file:
				try:
					v = json.load(conf_file)

					try:
						x = v["files"]
						y = v["folders"]
					except KeyError:
						print('Invalid configuration file. Please check the Archy README or run "archy -c reset"')
						quit()
					
					return v
				except json.decoder.JSONDecodeError:
					print(f'JSONDecodeError: Unable to read {self.filename}. Please ensure the JSON is valid.')
					quit()
		except FileNotFoundError:
			print('Configuration missing. Please run "archy -c create"')
			quit()
	
	def _not_found(self):
		print(f'{self.filename} was not found in: {self.config_location}')

		if input("Create it? (y/n) ") == "y":
			self.create()

	
	def create(self):
		self._check_config()
		self._create_config_file()

	def reset(self):
		if os.path.exists(self.config_file):
			self._confirm_reset()
		else:
			self._not_found()
	
	def edit(self):
		if os.path.exists(self.config_file):
			opener = input("Editor (<Enter> for vim, 'c' to cancel): ")

			if opener.strip() == "":
				opener = "vim"
			
			if opener.strip() == "c":
				quit()

			call([opener, self.config_file])
		else:
			self._not_found()

	def delete(self):
		try:
			self._confirm_reset()
		except FileNotFoundError:
			self._not_found()

	def echo(self):
		if os.path.exists(self.config_file):
			with open(self.config_file, 'r') as conf_file:
				print(conf_file.read())
		else:
			self._not_found()
	
	def does_conf_exist(self):
		return os.path.exists(self.config_file)