import os
import shutil
from pathlib import Path
from subprocess import call
from datetime import datetime

from .utils import copy_tree, make_tarfile, get_sha256
from .interface import ask

class Archiver:
	def __init__(self, config, settings):
		# This is the Config() instance itself
		self.config = config

		# This is the config object read from the file
		self.config_data = config.get_config()

		self.settings = settings
		self.home = Path.home()
		self.files = []
		self.dirs =[]

		self.today = datetime.today().strftime('%Y-%m-%d') # 1970-01-01
		self.archive_folder = os.path.join(self.home, f'archive-{self.today}') # /home/user/archive-1970-01-01
		self.files_folder = os.path.join(self.archive_folder, self.settings['file_folder_name']) # /home/user/archive-1970-01-01/FILES
		self.tarball = f'{self.archive_folder}.tar.gz' # /home/user/archive-1970-01-01.tar.gz

	def __call__(self):
		print('\nWelcome to Archy!\n')
		self.perform_initial_checks()
		self.confirm_archive()
	
	# Perform checks to see if the archive already exists.
	# If so, quit()
	def perform_initial_checks(self):
		should_fail = False
		paths = [self.archive_folder, self.tarball, f'{self.tarball}.gpg']
		for path in paths:
			if os.path.exists(path):
				print(f'The path "{path}" already exists. Please delete it to continue.')
				should_fail = True

		if should_fail: quit()

	# Pretty-print files or folders 
	def print_resources(self, resource):
		print(f'\n{resource.capitalize()}:')
		for line in self.config_data[resource]:
			print(f'- {line}')

	# Lists files and folders found in the config
	# file and asks if we should proceed with the
	# archive.
	#
	# If not, we ask if they want to edit the conf
	# file.
	#
	# If so, call self.go()
	def confirm_archive(self):
		if not self.settings['proceed']:
			print('From your config file, we found:')

			types = ['files', 'folders']
			for t in types:
				if self.config_data[t]:
					self.print_resources(t)

		if self.settings['proceed'] or ask('\nProceed? (y/n) '):
			self.go()
		else:
			if ask(f'Do you want to edit the {self.config.filename} file? (y/n) '):
				self.config.edit()

	# The primary operation method; only invoked when
	# the user confirms the values.
	#
	# It's long, I know, but it's mostly print statements.
	def go(self):
		did_compress = False
		did_encrypt = False
		
		self.create_archive_folder()
		self.split_config()
		self.copy_files()
		self.copy_dirs()

		print(f'\nAll files and folders have been copied!\n')

		if not self.settings['compress']:
			print("Do you want to compress the archive?")
		
		if self.settings['compress'] or ask("y to compress, n to leave as folder: "):
			did_compress = True
			self.compress_archive()
		
			if not self.settings['no_encrypt']:
				if self.settings['encrypt'] or ask("Do you want to encrypt the archive with GPG? (y/n) "):
					did_encrypt = True
					self.encrypt_archive()

		print('\n##############################\n')

		print('All operations have been completed!\n')
		
		print('Archive Folder:')
		print(f'- Location: {self.archive_folder}\n')
		
		if did_compress:
			print('Tarball:')
			print(f'- Location: {self.tarball}')
			print(f'- SHA256: {get_sha256(self.tarball)}')
		
		if did_encrypt:
			print('\nGPG Archive:')
			print(f'- Location: {self.tarball}.gpg')
			print(f'- SHA256: {get_sha256(f"{self.tarball}.gpg")}')

		print('\nThanks for using Archy!')

	##########

	# Creates the primary archive folder.
	# Quits if it already exists or if there
	# was a random error.
	def create_archive_folder(self):
		if os.path.exists(self.archive_folder):
			print(f'Archive {self.archive_folder} already exists.')
			quit()

		try:
			os.makedirs(self.archive_folder)
			os.makedirs(self.files_folder)
			print(f'\nCreated archive directory.\n')
		except:
			print('Unknown error attempting to create archive folder.')
			quit()

	# Read the config and append the values to
	# class variables.
	def split_config(self):
		for line in self.config_data["files"]:
			self.files.append(line)
		for line in self.config_data["folders"]:
			self.dirs.append(line)
	
	# Copy individual files using shutil
	# to the /FILES directory
	def copy_files(self):
		for f in self.files:
			print(f'Copying {os.path.basename(f)}')

			try:
				shutil.copy(f, self.files_folder)
			except:
				print(f'An unknown error occured trying to copy "{os.path.basename(f)}"')

	# Use the `cp` command to copy directories
	# to the primary archive directory.
	#
	# Using `cp` is insanely fast and so much more
	# reliable than other methods I've used in the
	# past.
	def copy_dirs(self):
		for d in self.dirs:
			print(f'Copying {os.path.basename(d)}/')
			copy_tree(d, self.archive_folder)

	# Compress the primary archive folder into a
	# tarball.
	def compress_archive(self):
		print('\nCreating tarball. This may take some time...')
		self.tarball = os.path.join(self.home, f'archive-{self.today}.tar.gz')
		
		try:
			make_tarfile(self.tarball, self.archive_folder)
			print(f'Tarball created!\n')
		except:
			print('There was an error creating the archive. Please delete the archive and try again.')
	
	# Use the `gpg` command to password-encrypt
	# the tarball.
	def encrypt_archive(self):
		try:
			call(['gpg', '-c', self.tarball])
		except:
			print('There was an error encrypting the tarball with GPG. Make sure it is installed.')
