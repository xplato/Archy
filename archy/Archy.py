import click

from .lib.main import Archy
from .lib.config import Config

settings = {}

def modify_setting(name, value):
	settings.update({ name: value })

@click.command()
@click.option('-c', '--conf', default=None, help='TEXT one of create, reset, edit, delete, or echo')
@click.option('-p', '--proceed', is_flag=True, default=False, help='Automatically archive without being prompted.')
@click.option('-x', '--compress', is_flag=True, default=False, help='Automatically compress without being prompted.')
@click.option('-e', '--encrypt', is_flag=True, default=False, help='Automatically encrypt (will still be prompted by GPG for password).')
@click.option('-n', '--no-encrypt', is_flag=True, default=False, help='Do not prompt for GPG encryption.')
@click.option('-f', '--file-folder-name', default='FILES', help='Name of the folder within the archive that contains files. Default is "FILES"')
@click.option('-d', '--devmode', is_flag=True, default=False, help='Enable DEV mode')
def cli(conf, proceed, compress, encrypt, no_encrypt, file_folder_name, devmode):
	config = Config(DEV=devmode)

	# This maps commands specific to the config file
	conf_options = {
		'create': config.create,
		'reset': config.reset,
		'edit': config.edit,
		'delete': config.delete,
		'echo': config.echo
	}

	if conf:
		try:
			conf_options[conf]()
			return
		except KeyError:
			print(f'"{conf}" is not a configuration command.')
			quit()
	
	modify_setting('devmode', devmode)
	modify_setting('compress', compress)
	modify_setting('encrypt', encrypt)
	modify_setting('no_encrypt', no_encrypt)
	modify_setting('file_folder_name', file_folder_name)
	modify_setting('proceed', proceed)

	archy = Archy(config, settings)
	archy()