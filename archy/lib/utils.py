import tarfile
from os import path
from hashlib import sha256
from subprocess import call

# I know, I know.
# This isn't "Pythonic" but it works.
# Using the copy_tree method from distutils
# doesn't work properly because it will skip
# random files that it can't handle.
#
# Whereas the `cp` command has proven to be
# consistent and reliable for over 50 years:
# https://en.wikipedia.org/wiki/Cp_(Unix)
def copy_tree(source, target):
    call(['cp', '-a', source, target])

def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=path.basename(source_dir))

def get_sha256(filename):
	# Python is pretty cool
	with open(filename, "rb") as f:
		bytes = f.read()
		readable_hash = sha256(bytes).hexdigest()
		return readable_hash