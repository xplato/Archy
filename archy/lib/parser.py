'''
Handles BASIC parsing of the archy.json file.
'''

import os

class Parser:
	def __init__(self):
		self.config = None
	
	def _verify_path(self, path):
		# Make sure the requested path exists
		if not os.path.exists(path):
			print(f'No such file or directory: "{path}"')
			print('Please run "archy -c edit" to edit the config file and remove the path that does not exist')
			quit()

	def _verify_values(self, config):
		for line in config:
			self._verify_path(line)

	def parse(self, config):
		if len(config["files"]) == 0 and len(config["folders"]) == 0:
			print('No files or folders specified. Please add a file or folder with "archy -c edit"')
			quit()

		try:
			self._verify_values(config["files"])
			self._verify_values(config["folders"])
		except KeyError:
			print('Corrupted config. Please reset it with "archy -c reset"')
			quit()