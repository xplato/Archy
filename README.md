# Archy

Archy is an automated backup program that takes a list of files and folders and duplicates them to a single tarball (with optional GPG encryption).

## Usage Overview

1. Install Archy
2. Edit the `archy.json` file with `archy -c edit`
3. Add your files and folders to the JSON file, then
4. Run `archy` to begin the archive in interactive mode. See the Documentation section below for information about completely autonomous archives.
5. In interactive mode:
	1. Archy will show you the files and folders it plans to compress and ask you if everything looks right.
	2. You'll be asked if you want to compress the archive folder into a tarball.
		- If yes to the above, you'll be asked if you want to encrypt the tarball with GPG.

### Notes & Features

- Archy uses system-level functions to make your archives, so it's generally pretty quick.
- Files are stored in a directory called `FILES` within the archive (this can be changed with the `-f` flag).
- Nothing is deleted during the process. If you both compress and encrypt your archive, you should end up with three new items in your home folder:
	- The archive folder
	- The compressed tarball
	- The GPG encrypted tarball
	- (I may later add an option to delete the previous items, but it's not a big deal now)
- The SHA256 hash of the tarball and GPG archive is displayed by default.

## Installation

You can either:

- Use `pip3` to install the `.whl` file in the [`dist/` folder](https://codeberg.org/athena/Archy/src/branch/main/dist)
- Build from source with `poetry`

### Basic Requirements

- Python is obviously required >= 3.10.x
- UNIX-like systems (GNU/Linux, MacOS) only, sorry :/

### Installing from the `.whl` file

Download the `.whl` file from the [`dist/` folder](https://codeberg.org/athena/Archy/src/branch/main/dist) and then run:

```
pip3 install --user archy-1.0.0-py3-none-any.whl
```

Note: Don't forget to modify the version number in the above command.

Close and re-open your terminal and the `archy` command should be available.

### Building from source

This project uses [Poetry](https://python-poetry.org/) to maintain the virtual environment. If you don't have it, I highly recommend it. Python's package system is atrocious and Poetry makes it... artful. I recommend installing it with `pipx`. See the [Poetry Install Section](https://python-poetry.org/docs/master/#installing-with-pipx) for information about that (and alternative installation methods).

1. `git clone https://codeberg.org/athena/Archy.git`
2. Install dependencies and build: `poetry install && poetry build` This will create a `dist/` folder.
3. `cd dist/ && ls`
4. Take the `.whl` file and run: `pip3 install --user archy-x.x.x-py3-none-any.whl`
5. Close or refresh your terminal and the `archy` command should be available.

If you have any trouble getting it to work (first make sure Python is on your `PATH`), follow the Development intructions near the bottom of this document.

## Updates

Installing updates is essentially the same, except use this command instead for step 4:

```
pip3 install --user archy-x.x.x-py3-none-any.whl --force-reinstall
```

## Documentation

If you run `archy` with no flags, it defaults to interactive mode. You'll be asked to confirm the initial archive, then asked if you want to create a tarball, and if so to the last one, you'll be asked if you want to encrypt the archive.

Since some people (like me) may want to add `archy` as a cron job, I've added some flags to make the process completely automatic.

Here is the `--help` content:

```
Usage: archy [OPTIONS]

Options:
  -c, --conf TEXT              TEXT one of create, reset, edit, delete, or echo
  -p, --proceed                Automatically archive without being prompted.
  -x, --compress               Automatically compress without being prompted.
  -e, --encrypt                Automatically encrypt (will still be prompted by GPG for password).
  -n, --no-encrypt             Do not prompt for GPG encryption.
  -f, --file-folder-name TEXT  Name of the folder within the archive that contains files. Default is "FILES"
  -d, --devmode                Enable DEV mode
  --help                       Show this message and exit.
```

In short, you can run Archy in fully-automatic (non-GPG) mode with:

```
archy -pxn
```

Or in fully-automatic mode with GPG encryption:

```
archy -pxe
```

### Additional flags

- Pass `-d` to enable development mode. Right now, this only makes Archy use the `archy-dev.json` file instead.
- Pass `-f` and an argument to change the name of the `FILES` directory.
	- For example: `archy -f dotfiles`

## Development

Poetry makes it super easy.

- Run `poetry install` to install dependencies
- Run `poetry shell` to initialize the virtualenv
- Run `poetry build` to build the package
- After this, `archy` should be available within the virtual environment.

## Technical Specifications

I'm using classes only to organize functions. That's it. I know it may not be best practice, but I don't care. It works fine, it makes nearly every single line very explicit (you know where a function is coming from 9/10 times), and so on.